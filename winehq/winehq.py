# -*- coding: iso-8859-1 -*-
"""
    MoinMoin theme for WineHQ.
"""

from MoinMoin import wikiutil
from MoinMoin.Page import Page
from MoinMoin.theme import ThemeBase

class Theme(ThemeBase):
    """ here are the functions generating the html responsible for
        the look and feel of your wiki site
    """

    name = "winehq"

    def title(self, d):
        """ Assemble the title

        @param d: parameter dictionary
        @rtype: string
        @return: title html
        """
        _ = self.request.getText
        if d['title_link']:
            content = ('<a title="%(title)s" href="%(href)s">%(text)s</a>') % {
                'title': _('Click to do a full-text search for this title'),
                'href': d['title_link'],
                'text': wikiutil.escape(d['title_text']),
                }
        else:
            content = wikiutil.escape(d['title_text'])

        return '''<div id="logo_blurb">%s</div>''' % content

    def msg(self, d):
        """ Assemble the msg display

        Display a message with a widget or simple strings with a clear message link.

        @param d: parameter dictionary
        @rtype: unicode
        @return: msg display html
        """
        _ = self.request.getText
        msg = d['msg']
        if not msg:
            return u''

        if isinstance(msg, (str, unicode)):
            # Render simple strings with a close link
            close = d['page'].link_to(self.request,
                                      text=_('Clear message'),
                                      querystr={'action': 'show'})
            msgbox = [
                u'<div class="clearmsg">',
                close,
                u'</div>',
                u'<p>%s</p>' % (msg)
                ]
            html = u'\n'.join(msgbox)

        else:
            # msg is a widget
            html = msg.render()

        return u'<div id="message">\n%s\n</div>\n' % html

    def searchform(self, d):
        """
        assemble HTML code for the search forms

        @param d: parameter dictionary
        @rtype: unicode
        @return: search form html
        """
        _ = self.request.getText
        form = self.request.form
        updates = {
            'search_label' : _('Search:'),
            'search_value': wikiutil.escape(form.get('value', [''])[0], 1),
            'search_full_label' : _('Text', formatted=False),
            'search_title_label' : _('Titles', formatted=False),
            }
        d.update(updates)

        html = [
            u'<div id="search_box">',
            u'  <form id="searchform" method="get" action="">',
            u'    <input type="hidden" name="action" value="fullsearch">',
            u'    <input type="hidden" name="context" value="180">',
            u'    <span>Moinmoin Title Search:</span>',
            u'''  <input id="searchinput" type="text" name="value"
                    value="''' + d['search_value'] + u'''"
                    onfocus="searchFocus(this)" onblur="searchBlur(this)"
                    onkeyup="searchChange(this)" onchange="searchChange(this)"
                    alt="Moinmoin Title Search">''',
            u'  </form>',
            u'</div>',
            u'<script type="text/javascript">',
            u'<!--// Initialize search box',
            u"  var f = document.getElementById('searchform');",
            u"  var e = document.getElementById('searchinput');",
            u"  searchChange(e); searchBlur(e);",
            u'//--> </script>',
            ]

        return u'\n'.join(html)

    def editbar(self, d):
        """ Assemble the page edit bar.

        Display on existing page. Replace iconbar, showtext, edit text,
        refresh cache and available actions.

        @param d: parameter dictionary
        @rtype: unicode
        @return: iconbar html
        """
        page = d['page']
        if not self.shouldShowEditbar(page):
            return ''

        # Use cached editbar if possible.
        cacheKey = 'editbar'
        cached = self._cache.get(cacheKey)
        if cached:
            return cached

        # Make new edit bar
        request = self.request
        _ = self.request.getText
        link = wikiutil.link_tag
        quotedname = wikiutil.quoteWikinameURL(page.page_name)
        links = []
        add = links.append

        # Parent page
        parent = page.getParentPage()
        if parent:
           add(parent.link_to(request, _("Show Parent", formatted=False)))

        available = request.getAvailableActions(page)

        # Page actions
        if page.isWritable() and request.user.may.write(page.page_name):
            add(link(request, quotedname + '?action=edit', _('Edit')))
        else:
            add(_('<i>Immutable Page</i>', formatted=False))

        add(link(request, quotedname + '?action=diff',
                 _('Show Changes', formatted=False)))
        add(link(request, quotedname + '?action=info',
                 _('Get Info', formatted=False)))
        add(self.subscribeLink(page))

        #    'SpellCheck': _('Check Spelling', formatted=False), # rename action!
        #    'LikePages': _('Show Like Pages', formatted=False),
        #    'LocalSiteMap': _('Show Local Site Map', formatted=False),

        add(link(request, quotedname + '?action=raw',
                 _('Show Raw Text', formatted=False)))
        add(link(request, quotedname + '?action=print',
                 _('Show Print View', formatted=False)))

        if ('AttachFile' in available):
            add(link(request, quotedname + '?action=AttachFile',
                     _('Attach File', formatted=False)))
        if ('RenamePage' in available):
            add(link(request, quotedname + '?action=RenamePage',
                     _('Rename Page', formatted=False)))
        if ('DeletePage' in available):
            add(link(request, quotedname + '?action=DeletePage',
                     _('Delete Page', formatted=False)))
        if page.canUseCache():
            add(link(request, quotedname + '?action=refresh',
                     _('Delete Cache', formatted=False)))

        # Format
        items = '\n'.join(['<li>%s</li>' % item for item in links if item != ''])
        html = u'<ul id="editbar">\n%s\n</ul>\n' % items

        # cache for next call
        self._cache[cacheKey] = html
        return html

    def sidetitle(self, title):
        _ = self.request.getText

        return '''<div class="sidetitle"><p>%s</p></div>''' % title

    def winetabs(self):
        """ Create wiki panel """
        _ = self.request.getText
        html = [
            u'<div id="tabs">',
            u'  <ul>',
            u'    <li><a href="http://www.winehq.org/">WineHQ</a></li>',
            u'    <li class="current"><a href="http://wiki.winehq.org/">Wiki</a></li>',
            u'    <li><a href="http://appdb.winehq.org/">AppDB</a></li>',
            u'    <li><a href="http://bugs.winehq.org/">Bugzilla</a></li>',
            u'    <li><a href="http://forum.winehq.org/">Forums</a></li>',
            u'  </ul>',
            u'</div>',
            ]
        return u'\n'.join(html)

    def wikipanel(self, d):
        """ Create wiki panel """
        _ = self.request.getText
        html = [
            u'<div class="sidepanel">',
            self.sidetitle(_("Wiki Links")),
            u'  <div class="sidecontent">',
            self.navibar(d),
            u'  </div>',
            u'  <div class="sidebottom"></div>',
            u'</div>',
            ]
        return u'\n'.join(html)

    def pagepanel(self, d):
        """ Create page panel """
        _ = self.request.getText
        if self.shouldShowEditbar(d['page']):
            html = [
                u'<div class="sidepanel">',
                self.sidetitle(_("Page Tools")),
                u'  <div class="sidecontent">',
                self.editbar(d),
                u'  </div>',
                u'  <div class="sidebottom"></div>',
                u'</div>',
                ]
            return u'\n'.join(html)
        return ''

    def userpanel(self, d):
        """ Create user panel """
        request = self.request
        _ = request.getText

        #trail = self.trail(d)
        #if trail:
        #    trail = u'<h2>%s</h2>\n' % _("Trail") + trail

        html = [
            u'<div class="sidepanel">',
            self.sidetitle(_("User Tools")),
            u'  <div class="sidecontent">',
            self.username(d),
            #trail,
            u'  <ul id="pagetrail">',
            u'    <li>%s</li>' % Page(request, 'WikiSandBox').link_to(request, 'Wiki Sand Box'),
            u'    <li>%s</li>' % Page(request, 'SyntaxReference').link_to(request, 'Syntax Reference'),
            u'  </ul>',
            u'  </div>',
            u'  <div class="sidebottom"></div>',
            u'</div>'
            ]
        return u'\n'.join(html)

    def header(self, d):
        """
        Assemble page header

        @param d: parameter dictionary
        @rtype: string
        @return: page header html
        """
        _ = self.request.getText

        html = [
            # Custom html above header
            self.emit_custom_html(self.cfg.page_header1),

            # Header
            u'<div id="header">',
            self.logo(),
            self.title(d),
            self.winetabs(),
            self.searchform(d),
            u'</div>',

            # Custom html below header (not recomended!)
            self.emit_custom_html(self.cfg.page_header2),

            # Sidebar
            u'<div id="sidewell">',
            u'  <div id="sidebar">',
            self.wikipanel(d),
            self.pagepanel(d),
            self.userpanel(d),
            u'  </div>',
            u'</div>',

            # Page
            u'<div id="pagewell">',
            u'  <div id="pagecontent"%s>' % self.content_lang_attr(),
            self.msg(d),
            ]
        return u'\n'.join(html)

    def footer(self, d, **keywords):
        """ Assemble page footer

        @param d: parameter dictionary
        @keyword ...:...
        @rtype: string
        @return: page footer html
        """
        page = d['page']
        html = [
            # Page end
            # Used to extend the page to the bottom of the sidebar
            u'  <div id="pagebottom"></div>',
            self.pageinfo(page),

            u'  </div> <!-- end pagecontent -->',
            u'  <div>', self.credits(d), u'</div>',
            u'</div> <!-- end pagewell -->',

            # Custom html above footer
            self.emit_custom_html(self.cfg.page_footer1),

            # And below
            self.emit_custom_html(self.cfg.page_footer2),
            ]
        return u'\n'.join(html)

def execute(request):
    """
    Generate and return a theme object

    @param request: the request object
    @rtype: MoinTheme
    @return: Theme object
    """
    return Theme(request)
